from django.db import models

# Create your models here.
class Donasi(models.Model):
    nama = models.CharField(max_length=50)
    pesan_singkat = models.CharField(max_length=100)
    jumlah = models.DecimalField(max_digits=8, decimal_places=2)
    tanggal = models.DateField(auto_now_add = True)