from django.urls import path
from . import views

app_name = 'home'

urlpatterns = [
    path('', views.home_views, name='home'),
    path('dataProvJson/', views.dataProvJsonResponse , name='provJson')
]