$(document).ready(function () {    
    $("#prov-location").click(function () { 
        var provName = $("#prov-value").val();
        $.ajax({
            url: "dataProvJson/",
            dataType: "json",
            success: function (response) {
                var dataDetails = response[provName]
    
                $("#prov-name").html(provName);
                $("#data-confirmed").html(dataDetails["confirmed"]);
                $("#data-recovered").html(dataDetails["recovered"]);
                $("#data-death").html(dataDetails["death"]);
                $("#provinsi-selector").modal('hide');
            }
        });
    });


    $(".judul").mouseenter(function () { 
        $(this).css({
            "color": "#fff", "text-shadow": "0 0 5px #fff" });
    }).mouseleave(function () { 
        $(this).css({
            "color": "#fff", "text-shadow": "" });
    });

});





