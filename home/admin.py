from django.contrib import admin

from .models import DataCovidIndonesia, DataProvinsi

admin.site.register(DataCovidIndonesia)
admin.site.register(DataProvinsi)