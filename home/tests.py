from django.test import TestCase
from .models import DataCovidIndonesia, DataProvinsi
import requests
from django.utils import timezone

class HomeUnitTest(TestCase):

    def test_root_url_status_200(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200) 
    
    def test_if_data_exist(self):
        self.client.get('/')
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200) 

    def test_POST_request(self):
        self.client.get('/')
        response = self.client.post('/', data={"provinsi" : "Jawa Barat"})
        html_response = response.content.decode("utf8")

    def test_tostring_model(self):
        self.client.get('/')
        prov = DataProvinsi.objects.get(nama="Papua Barat")
        provStr = prov.__str__()
        self.assertEquals("Papua Barat", provStr)
    
    def test_jsonResponse(self):
        self.client.get('/')
        response = self.client.get("/dataProvJson/")
        self.assertEqual(response.status_code, 200)


        





        

