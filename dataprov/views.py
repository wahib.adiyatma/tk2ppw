from django.shortcuts import render, redirect
import requests
from django.utils import timezone
from .models import dataCovidProvinsi, SearchBar
from django.http import JsonResponse
from .forms import SearchBarForm


def index_dataprov(request):
	search_form = SearchBarForm(request.POST or None)
	

	dataResp = requests.get('https://data.covid19.go.id/public/api/prov.json')
	dataJson = dataResp.json()
	isi_data = dataJson['list_data']
	last_date = dataJson['last_date']
	if not dataCovidProvinsi.objects.all().exists(): 
		for data_prov in isi_data :
			datacovidprov = dataCovidProvinsi.objects.create(date=last_date,
				provinsi = data_prov['key'],
				jumlah_kasus = data_prov['jumlah_kasus'],
				jumlah_dirawat = data_prov['jumlah_dirawat'],
				jumlah_sembuh = data_prov['jumlah_sembuh'],
				jumlah_meninggal = data_prov['jumlah_meninggal'],
				penambahan_positif = data_prov['penambahan']['positif'],
				penambahan_sembuh = data_prov['penambahan']['sembuh'],
				penambahan_meninggal = data_prov['penambahan']['meninggal']
				)

	
	for data_prov_baru in isi_data:
		datacovidprov = dataCovidProvinsi.objects.get(provinsi=data_prov_baru['key'])
		datacovidprov.date = last_date
		datacovidprov.jumlah_kasus = data_prov_baru['jumlah_kasus']
		datacovidprov.jumlah_dirawat= data_prov_baru['jumlah_dirawat']
		datacovidprov.jumlah_sembuh= data_prov_baru['jumlah_sembuh']
		datacovidprov.jumlah_meninggal= data_prov_baru['jumlah_meninggal']
		datacovidprov.penambahan_positif= data_prov_baru['penambahan']['positif']
		datacovidprov.penambahan_sembuh= data_prov_baru['penambahan']['sembuh']
		datacovidprov.penambahan_meninggal= data_prov_baru['penambahan']['meninggal']
		datacovidprov.save()

	context={
	'search_form' : search_form,
	'date': last_date,
	}


	return render(request, 'dataprov/index.html', context);


def dataJson(request):
	response={}
	dataProv = dataCovidProvinsi.objects.all()
	for eachProv in dataProv :
		context={}
		context['jumlah_kasus'] = eachProv.jumlah_kasus
		context['jumlah_dirawat'] = eachProv.jumlah_dirawat
		context['jumlah_sembuh'] = eachProv.jumlah_sembuh
		context['jumlah_meninggal'] = eachProv.jumlah_meninggal
		context['penambahan_positif'] = eachProv.penambahan_positif
		context['penambahan_sembuh'] = eachProv.penambahan_sembuh
		context['penambahan_meninggal'] = eachProv.penambahan_meninggal
		response[eachProv.provinsi] = context


	return JsonResponse(response)


