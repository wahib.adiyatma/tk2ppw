from django.test import TestCase, Client
from .models import dataCovidProvinsi, SearchBar
import requests
import json

class DataProvUnitTest(TestCase):

    def test_url(self):
        response = self.client.get('/dataprov/')
        self.assertEqual(response.status_code, 200) 
    
    def test_data_exist(self):
        self.client.get('/dataprov/')
        response = self.client.get('/dataprov/')
        self.assertEqual(response.status_code, 200) 
    
    def test_jsonResponse(self):
        self.client.get('/dataprov/')
        response = self.client.get("/dataprov/jsonCall/")
        self.assertEqual(response.status_code, 200)

    def test_dataprov_url_is_resolved(self):
        response = Client().get('/dataprov/jsonCall/')
        self.assertEquals(response.status_code, 200)

    def test_tostring_model(self):
        self.client.get('/dataprov/')
        obj = dataCovidProvinsi.objects.get(provinsi="BALI")
        objStr = obj.__str__()
        self.assertEquals("BALI", objStr)

    def test_POST_request(self):
        self.client.get('/dataprov/')
        response = self.client.post('/dataprov/', data={"provinsi" : "JAMBI"})
        html_response = response.content.decode("utf8")

    def test_dataprovajax_url_is_exist(self):
        response = self.client.get('/dataprov/jsonCall/')
        isi = response.content.decode('utf8')
        self.assertIn('{}',isi)



        

