$(document).ready(function () {    
    $("#search_button").click(function () { 
        var input = $("#search_input").val().toUpperCase();
        $('#result_box').show();
        $.ajax({
            url: "/dataprov/jsonCall",
            dataType: "json",
            success: function (response) {
                var dataResponse = response[input];
                var provinsi = input;
                var jumlah_kasus = dataResponse['jumlah_kasus'];
                var jumlah_sembuh = dataResponse['jumlah_sembuh'];
                var jumlah_dirawat = dataResponse['jumlah_dirawat'];
                var jumlah_meninggal = dataResponse['jumlah_meninggal'];
                var penambahan_sembuh = dataResponse['penambahan_sembuh'];
                var penambahan_positif = dataResponse['penambahan_positif'];
                var penambahan_meninggal = dataResponse['penambahan_meninggal'];

                $('#title_result').html("DATA STATISTIK COVID-19 PROVINSI : "+ provinsi );
                $('#total_kasus').html(jumlah_kasus);
                $('#jumlah_sembuh').html(jumlah_sembuh);
                $('#jumlah_dirawat').html(jumlah_dirawat);
                $('#jumlah_meninggal').html(jumlah_meninggal);
                $('#penambahan_sembuh').html(penambahan_sembuh);
                $('#penambahan_positif').html(penambahan_positif);
                $('#penambahan_meninggal').html(penambahan_meninggal);
            }
        });
    });



});





