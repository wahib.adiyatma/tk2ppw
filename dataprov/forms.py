from django import forms
from .models import SearchBar

class SearchBarForm(forms.ModelForm):
	class Meta :
		model = SearchBar
		fields = [
			'search',
		]

		widgets = {
		'search' : forms.TextInput(
				attrs={
					'class':'form-control',
					'id':'search_input',
					'style' : 'border-radius:15px; height: 40px;',
					'placeholder':'Cari data Covid-19 terkini berdasarkan provinsi...'
				}
			),
		}
