from django.db import models

class SearchBar(models.Model):
	search = models.CharField(max_length=50)	
	
class dataCovidProvinsi(models.Model):
	date = models.DateField()
	provinsi = models.CharField(max_length = 100)
	jumlah_kasus = models.CharField(max_length = 200)
	jumlah_dirawat = models.CharField(max_length = 200)
	jumlah_sembuh = models.CharField(max_length = 200)
	jumlah_meninggal = models.CharField(max_length = 200)
	penambahan_positif = models.CharField(max_length = 200)
	penambahan_sembuh = models.CharField(max_length = 200)
	penambahan_meninggal = models.CharField(max_length = 200)

	class Meta:
		ordering = ['provinsi']

	def __str__(self):
		return self.provinsi




