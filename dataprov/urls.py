from django.urls import include, path
from . import views

urlpatterns = [
    path('', views.index_dataprov, name='index_dataprov'),
    path('jsonCall/', views.dataJson, name='jsonCall')
    ]
