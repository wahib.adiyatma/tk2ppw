from django.urls import path

from . import views

app_name = 'rumahsakit'

urlpatterns = [
    path('home/', views.home, name = 'rumahsakit'),
    path('tambah/', views.addHospital, name = 'tambah'),
    path('search/', views.searchHospital, name = 'search')
]