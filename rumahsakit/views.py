from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.core import serializers
from .models import *
from .forms import *

def home(request):
    rumahsakit = Rumahsakit.objects.all()
    return render(request, 'template/rumahsakit.html', {'data':rumahsakit})

def addHospital(request):
    nama = request.POST.get('nama', None)
    lokasi = request.POST.get('lokasi',None)
    waktu = request.POST.get('waktu', None)
    notelp = request.POST.get('notelp', None)
    website = request.POST.get('website', None)
    maps = request.POST.get('maps', None)
    foto = request.POST.get('foto', None)

    Rumahsakit.objects.create(nama = nama, lokasi = lokasi, waktu = waktu, notelp = notelp, website = website, maps = maps, link_foto = foto)
    return redirect('/rumahsakit')


def searchHospital(request):
    if (request.is_ajax()):
        rs = Rumahsakit.objects.filter(nama__contains = request.GET['searchBar'])
        rs_api = serializers.serialize('json', rs)
        return HttpResponse(rs_api, content_type="text/json-comment-filtered")



