from django.test import TestCase


# Create your tests here.
from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.http import HttpRequest, response
from .views import *

class test_path(TestCase):
    def test_path(self):
        response = Client().get('/login/',{},True)
        self.assertEqual(response.status_code,200)
    def test_path2(self):
        response = Client().get('/signup/',{},True)
        self.assertEqual(response.status_code,200)
    def test_path3(self):
        response = Client().get('/logout/',{},True)
        self.assertEqual(response.status_code,200)

class text_input(TestCase):
    def test_textHTMLLogin(self):
        response = Client().get('/login/')
        html = response.content.decode('utf-8')
        self.assertIn("username", html)
        self.assertIn('password',html)
    def test_textHTMLDaftar(self):
        response = Client().get('/signup/')
        html = response.content.decode('utf-8')
        self.assertIn("username",html)
        self.assertIn("password", html)
        self.assertIn("email", html)

class test_urls(TestCase):
    def setUp(self):
        self.login = reverse("ourAuth:login")
        self.signup = reverse("ourAuth:signup")
        self.logout = reverse("ourAuth:logout")
    
    def test_login_use_right_function(self):
        found = resolve(self.login)
        self.assertEqual(found.func, loginUser)

    def test_signup_use_right_function(self):
        found = resolve(self.signup)
        self.assertEqual(found.func, createUser)

    def test_logout_use_right_function(self):
        found = resolve(self.logout)
        self.assertEqual(found.func, logoutUser)

class UserCreationFormTest(TestCase):
    def test_form(self):
        data = {
            'username': 'fahmifd',
            'email':'pahme@gmail.com',
            'password1': 'testz213',
            'password2': 'testz213',
        }
        form = UserCreationForm(data)
        self.assertTrue(form.is_valid())

    def test_post_signup_login(self):
        createUserResp = self.client.post('/signup/', data={
            'username': 'fahmifd',
            'email':'pahme@gmail.com',
            'password1': 'testz213',
            'password2': 'testz213',
        })
        self.assertRedirects(createUserResp, '/login/')

        loginUserResp = self.client.post('/login/', data={
            'username': 'fahmifd',
            'password': 'testz213',
        })
        self.assertRedirects(loginUserResp, '/')

        invalidLoginResp = self.client.post('/login/', data={
            'username': 'fahmifd',
            'password': 'testz21aaaaaa',
        })

        html = invalidLoginResp.content.decode('utf-8')
        self.assertIn("Username atau password salah" , html)
        


        

